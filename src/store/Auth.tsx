import { createSlice } from "@reduxjs/toolkit";
import jwtDecode from "jwt-decode";
import { RegistrationData } from "../models/interfaces/Registration";
import { Account } from "../models/interfaces/Account";
import { AuthState } from "../models/interfaces/AuthState";
import { LoginData } from "../models/interfaces/Login";
import { get } from "lodash-es";
import { getReasonPhrase } from "http-status-codes";
import { StatusCodes } from "http-status-codes";
import { apiCall } from "./Api2";

export const AUTH_TOKEN_KEY = "authorization";
const slice = createSlice({
  name: "auth",
  initialState: {
    loading: false,
    identity: undefined,
    error: undefined,
  },
  reducers: {
    authStarted: (auth) => {
      auth.loading = true;
      auth.error = undefined;
    },

    authSuccess: (auth: AuthState, { payload }) => {
      const { headers } = payload;
      loginWithToken(get(headers, AUTH_TOKEN_KEY));
      auth.identity = getAuthIdentity();
      console.log("Authentication details:", auth.identity);
    },

    authFailed: (auth: AuthState, { payload }) => {
      if (!payload) {
        auth.error = getReasonPhrase(StatusCodes.SERVICE_UNAVAILABLE);
        return;
      }
      auth.error = payload;
    },

    authReset: (auth: AuthState) => {
      auth.error = undefined;
    },

    authDone: (auth: AuthState) => {
      auth.loading = false;
    },

    authIdentity: (auth: AuthState) => {
      auth.identity = getAuthIdentity();
    },

    authLoggedOut: (auth: AuthState) => {
      console.log("logging out..");
      logout();
      auth.identity = undefined;
    },
  },
});

export const {
  authStarted,
  authSuccess,
  authFailed,
  authReset,
  authDone,
  authIdentity,
  authLoggedOut,
} = slice.actions;

export const register = (params: RegistrationData) =>
  apiCall({
    url: "/identity",
    method: "POST",
    postParams: params,
    onStart: authStarted.type,
    onSuccess: authSuccess.type,
    onError: authFailed.type,
    onDone: authDone.type,
  });

export const login = ({ email, password }: LoginData) =>
  apiCall({
    url: "/identity/login",
    method: "POST",
    postParams: { email, password },
    onStart: authStarted.type,
    onSuccess: authSuccess.type,
    onError: authFailed.type,
    onDone: authDone.type,
  });

export const getAuthIdentity = (): Account | any => {
  try {
    console.log("Logged in user..");
    return jwtDecode(getToken() as string);
  } catch (error) {
    console.log("Anonymous..", error);
    return false;
  }
};

export const logout = (): void => {
  localStorage.removeItem(AUTH_TOKEN_KEY);
};

export const loginWithToken = (token: string) => {
  localStorage.setItem(AUTH_TOKEN_KEY, token);
};

export const getToken = (): string | null => {
  return localStorage.getItem(AUTH_TOKEN_KEY);
};

export default slice.reducer;
