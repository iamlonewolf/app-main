import { FunctionComponent, useEffect } from "react";
import "./AuthenticationLanding.scss";
import Authentication, {
  AuthenticationMode,
} from "../Authentication/Authentication";
import { Redirect, RouteComponentProps, withRouter } from "react-router";
import { includes } from "lodash";
import {
  ROUTE_AUTHENTICATE_REGISTRATION,
  ROUTE_MONITOR_ROOT,
} from "../../routes";
import { ROUTE_AUTHENTICATE_LOGIN } from "./../../routes";
import { useSelector } from "react-redux";
import { hasIdentity } from "../../selectors/Auth";

interface AuthenticationLandingProps extends RouteComponentProps<any> {}

const AuthenticationLanding: FunctionComponent<AuthenticationLandingProps> = ({
  match,
  history,
}) => {
  const mode = match?.params?.mode;
  const isLoggedIn = useSelector(hasIdentity);

  const initialize = () => {
    if (
      !includes(
        [AuthenticationMode.LOGIN, AuthenticationMode.REGISTRATION],
        mode
      ) ||
      isLoggedIn
    ) {
      history.push(ROUTE_MONITOR_ROOT);
      return;
    }
  };
  useEffect(() => {
    initialize();
  }, [isLoggedIn]);

  const onClickLoginLink = () => {
    history.push(ROUTE_AUTHENTICATE_LOGIN);
  };

  const onClickRegisterLink = () => {
    history.push(ROUTE_AUTHENTICATE_REGISTRATION);
  };

  return (
    <div className="h-100 row p-3">
      <div className="col-md-5 offset-md-4 col-lg-4 offset-lg-4 col-sm-12 offset-sm-0 d-flex align-items-center">
        <div className="authentication-box rounded w-100 p-4">
          <Authentication
            mode={mode}
            onClickLoginLink={onClickLoginLink}
            onClickRegisterLink={onClickRegisterLink}
          />
        </div>
      </div>
    </div>
  );
};

export default withRouter(AuthenticationLanding);
