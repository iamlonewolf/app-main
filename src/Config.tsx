import { get } from "lodash-es";
export const getConfig = (key: string, defaultVal?: any) => {
  return get(process.env, key, defaultVal);
};
