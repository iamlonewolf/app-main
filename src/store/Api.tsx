import { createAction } from "@reduxjs/toolkit";
import { Method } from "axios";
export interface APIConfig {
  url?: string;
  method?: Method;
  postParams?: any;
  queryParams?: any;
  onStart?: any;
  onSuccess?: any;
  onError?: any;
  onDone?: any;
}

export const apiCallBegan = createAction<APIConfig>("api/callBegan");
export const apiCallSuccess = createAction("api/callSuccess");
export const apiCallError = createAction("api/callError");
export const apiCallDone = createAction("api/callDone");
