import { Entities } from "./Entities";
import { AuthState } from "./AuthState";

export interface RootState {
  entities?: Entities;
  auth?: AuthState;
}
