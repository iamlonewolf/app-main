import "./AccountProfile.scss";
import { joiResolver } from "@hookform/resolvers/joi";
import { Fragment, FunctionComponent, useEffect } from "react";
import { useForm } from "react-hook-form";
import { withRouter } from "react-router";
import { Account } from "../../models/interfaces/Account";
import { useDispatch, useSelector } from "react-redux";
import { getAuthIdentity } from "../../store/Auth";
import { fetchAccount } from "../../store/Account";
import {
  accountSchema,
  DEFAULT_MAX_LENGTH,
  DEFAULT_MIN_LENGTH,
} from "../../models/schemas/Account";
import { getAccountData } from "../../selectors/Account";
import { AppDispatch } from "../../store/ConfigureStore";
import Joi from "joi";

interface AccountProfileProps {}

const AccountProfile: FunctionComponent<AccountProfileProps> = () => {
  const dispatch = useDispatch<AppDispatch>();
  const identity = useSelector(getAuthIdentity);
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<Account>({
    resolver: joiResolver(accountSchema),
  });

  const onSubmit = handleSubmit(() => {
    alert(123123123);
  });

  useEffect(() => {
    dispatch(fetchAccount(identity?._id))
      .unwrap()
      .then(({ data: account }) => {
        reset(account);
      });
  }, [identity?._id]);

  console.log(errors);
  return (
    <Fragment>
      <h2 className="mb-3">
        <i className="fas fa-user-circle me-2"></i>
        <span>User Profile</span>
      </h2>
      <form onSubmit={onSubmit}>
        <div className="account-profile d-flex flex-column w-100">
          <div className="mb-2">
            <input
              type="text"
              className="form-control"
              placeholder="Enter your First name"
              {...register("firstName", {
                required: true,
              })}
            />
            {errors?.firstName?.type && (
              <span className="text-danger text-error">
                {errors?.firstName?.type === "string.empty" &&
                  "Last name is required."}
                {errors?.firstName?.type === "string.min" &&
                  `Last name should not be less than ${DEFAULT_MIN_LENGTH}`}
                {errors?.firstName?.type === "string.max" &&
                  `Last name should not be more than ${DEFAULT_MAX_LENGTH}`}
              </span>
            )}
          </div>
          <div className="mb-2">
            <input
              type="text"
              className="form-control"
              placeholder="Enter your Last name"
              {...register("lastName", {
                required: true,
              })}
            />
            {errors?.lastName?.type && (
              <span className="text-danger text-error">
                {errors?.lastName?.type === "string.empty" &&
                  "First name is required."}
                {errors?.lastName?.type === "string.min" &&
                  `First name should not be less than ${DEFAULT_MIN_LENGTH}`}
                {errors?.lastName?.type === "string.max" &&
                  `First name should not be more than ${DEFAULT_MAX_LENGTH}`}
              </span>
            )}
          </div>
          <div className="mb-2">
            <input
              type="email"
              className="form-control"
              id="email"
              placeholder="Enter your email"
              {...register("email", {
                required: true,
                maxLength: DEFAULT_MAX_LENGTH,
                pattern: {
                  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                  message: "Enter a valid e-mail address",
                },
              })}
            />
            {errors?.email?.type && (
              <span className="text-danger text-error">
                {errors?.email?.type === "string.empty" && "Email is required."}
                {errors?.email?.type === "string.email" && "Email is invalid."}
                {errors?.email?.type === "string.max" &&
                  `Email should not be more than ${DEFAULT_MAX_LENGTH}`}
              </span>
            )}
          </div>

          <button className="btn mb-2">Submit</button>
        </div>
      </form>
    </Fragment>
  );
};

export default withRouter(AccountProfile);
