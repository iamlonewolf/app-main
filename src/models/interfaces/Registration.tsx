import { Account } from "./Account";
export interface RegistrationData extends Account {
  name?: string;
  email: string;
  password: string;
}
