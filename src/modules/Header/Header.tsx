import "./Header.scss";
import { Fragment, FunctionComponent } from "react";
import { Link, RouteComponentProps } from "react-router-dom";
import {
  ROUTE_ACCOUNT_PROFILE_ROOT,
  ROUTE_APP_ROOT,
  ROUTE_AUTHENTICATE_LOGIN,
  ROUTE_AUTHENTICATE_REGISTRATION,
  ROUTE_MONITOR_ROOT,
} from "./../../routes";
import { getAuthIdentity, hasIdentity } from "../../selectors/Auth";
import { authIdentity, authLoggedOut } from "../../store/Auth";
import { withRouter } from "react-router";
import { useDispatch, useSelector } from "react-redux";

interface HeaderProps extends RouteComponentProps<any> {}

const Header: FunctionComponent<HeaderProps> = ({ history }) => {
  const dispatch = useDispatch();
  const isLoggedIn = useSelector(hasIdentity);
  const identity = useSelector(getAuthIdentity);
  const logout = () => {
    dispatch(authLoggedOut());
    dispatch(authIdentity());
    history.push(ROUTE_AUTHENTICATE_LOGIN);
  };
  return (
    <nav className="navbar navbar-expand-md fixed-top navbar-light bg-light mb-4">
      <div className="container-fluid">
        <Link to={ROUTE_APP_ROOT} className="navbar-brand">
          <i className="fas fa-keyboard"></i> AFK Tracker
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarCollapse"
          aria-controls="navbarCollapse"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarCollapse">
          <ul className="navbar-nav me-auto mb-2 mb-md-0">
            <li className="nav-item">
              <Link to={ROUTE_MONITOR_ROOT} className="text-reset">
                <span>Monitor</span>
              </Link>
            </li>
          </ul>
          {!isLoggedIn && (
            <Fragment>
              <Link to={ROUTE_AUTHENTICATE_LOGIN} className="text-reset me-2">
                Login
              </Link>
              <Link
                to={ROUTE_AUTHENTICATE_REGISTRATION}
                className="text-reset me-2"
              >
                Register
              </Link>
            </Fragment>
          )}
          {isLoggedIn && (
            <Fragment>
              {/*               <Link
                to={ROUTE_ACCOUNT_PROFILE_ROOT}
                className="me-2 text-reset profile-link d-flex align-items-center"
              >
                <i className="fas fa-user-circle me-2 fa-2x"></i>
                <span>{identity?.firstName}</span>
              </Link> */}
              <span className="me-2 text-reset profile-link d-flex align-items-center">
                <i className="fas fa-user-circle me-2 fa-2x"></i>
                <span>{identity?.firstName}</span>
              </span>
              <a onClick={() => logout()} className="text-reset me-2">
                Logout
              </a>
            </Fragment>
          )}
        </div>
      </div>
    </nav>
  );
};

export default withRouter(Header);
