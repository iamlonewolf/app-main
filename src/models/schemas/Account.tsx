import Joi from "joi";
import { Account } from "../interfaces/Account";
export const DEFAULT_MAX_LENGTH = 255;
export const DEFAULT_MIN_LENGTH = 1;

export const accountSchema = Joi.object<Account>({
  firstName: Joi.string().required().max(DEFAULT_MAX_LENGTH),
  lastName: Joi.string().required().max(DEFAULT_MAX_LENGTH),
  position: Joi.string().max(DEFAULT_MAX_LENGTH),
  department: Joi.string().max(DEFAULT_MAX_LENGTH),
  email: Joi.string().email({ tlds: { allow: false } }),
  password: Joi.string().required().min(DEFAULT_MIN_LENGTH),
});

export const accountLoginSchema = Joi.object<Account>({
  email: Joi.string().email({ tlds: { allow: false } }),
  password: Joi.string().required().min(DEFAULT_MIN_LENGTH),
});
