import {
  apiCallBegan,
  apiCallSuccess,
  apiCallError,
  apiCallDone,
} from "../store/Api";
import { Middleware } from "redux";
import { getConfig } from "../Config";
import { isEmpty } from "lodash-es";
import httpService from "../services/HttpService";

const api: Middleware =
  ({ dispatch }) =>
  (next) =>
  async (action) => {
    if (action.type !== apiCallBegan.type) {
      return next(action);
    }
    next(action);
    const {
      url,
      method,
      postParams,
      queryParams,
      onStart,
      onSuccess,
      onError,
      onDone,
    } = action.payload;

    if (onStart) {
      dispatch({
        type: onStart,
      });
    }

    try {
      const { data, headers } = await httpService.request({
        baseURL: getConfig("REACT_APP_API_URL"),
        url,
        method: method ?? "GET",
        data: postParams,
        params: queryParams,
      });
      if (onSuccess) {
        dispatch({
          type: onSuccess,
          payload: {
            data,
            headers,
          },
        });
      }
      dispatch({
        type: apiCallSuccess.type,
        payload: {
          data,
          headers,
        },
      });
    } catch (error: any) {
      const { response } = error;
      const responseData = !isEmpty(response?.data)
        ? response?.data
        : response?.statusText;
      if (onError) {
        dispatch({
          type: onError,
          payload: responseData,
        });
      }
      dispatch({
        type: apiCallError.type,
        payload: responseData,
      });
    } finally {
      if (onDone) {
        dispatch({ type: onDone });
      }

      dispatch({ type: apiCallDone.type });
    }
  };

export default api;
