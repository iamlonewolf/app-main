import { Account } from "./interfaces/Account";
import { ConnectionState } from "./interfaces/ConnectionState";

export class AccountConnectionItem {
  account?: Account;
  connectionState?: ConnectionState;
}
