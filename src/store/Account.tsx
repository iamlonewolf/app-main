import { createSlice } from "@reduxjs/toolkit";
import { Account } from "../models/interfaces/Account";
import { apiCall } from "./Api2";

const slice = createSlice({
  name: "account",
  initialState: {
    loading: false,
    data: undefined,
    list: undefined,
    error: undefined,
  },
  reducers: {
    accountsLoaded: (account, { payload }) => {},
    accountLoaded: (account, { payload }) => {
      account.data = payload?.data;
    },
    accountStarted: (account, { payload }) => {},
    accountFailed: (account, { payload }) => {},
    accountDone: (account, { payload }) => {},
  },
});

export const fetchAccount = (accountId: number) => {
  return apiCall({
    url: `/account/${accountId}`,
    method: "GET",
    onStart: accountStarted.type,
    onSuccess: accountLoaded.type,
    onError: accountFailed.type,
    onDone: accountDone.type,
  });
};

export const { accountStarted, accountLoaded, accountFailed, accountDone } =
  slice.actions;
export default slice.reducer;
