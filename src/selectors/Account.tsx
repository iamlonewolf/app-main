import { createSelector } from "@reduxjs/toolkit";
import { isEqual, isUndefined } from "lodash-es";
import { RootState } from "../models/interfaces/RootState";
import { AppState } from "../models/interfaces/AppState";

export const getAccount = ({ entities }: RootState) => entities?.account;

export const isAccountProcessing = ({ entities }: RootState) =>
  entities?.account?.loading;

export const getAccountError = ({ entities }: RootState) =>
  entities?.account?.error;

export const getAccountData = createSelector(
  ({ entities }: RootState): AppState | undefined => entities?.account,
  (account: AppState | undefined) => {
    if (!account) {
      return undefined;
    }
    return account?.data;
  }
);

export const getAccountId = createSelector(getAccountData, (account) => {
  return account?._id;
});
