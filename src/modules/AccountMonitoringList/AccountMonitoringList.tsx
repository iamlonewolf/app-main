import "./AccountMonitoringList.scss";
import { io, Socket } from "socket.io-client";
import { Fragment, FunctionComponent, useEffect, useState } from "react";
import { getConfig } from "./../../Config";
import { AccountConnectionItem } from "../../models/AccountConnectionItem";

interface AccountMonitoringListProps {}

const AccountMonitoringList: FunctionComponent<AccountMonitoringListProps> =
  () => {
    const [socket, setSocket] = useState<Socket | undefined>(undefined);
    const [connectionItemList, setAccountConnectionItemList] = useState<
      AccountConnectionItem[] | undefined
    >(undefined);
    useEffect(() => {
      const currentSocket = io(`${getConfig("REACT_APP_SITE_URL")}/admin`);
      // const currentSocket = io("https://service3.iot.johnsikstri.com/admin");
      setSocket(currentSocket);

      currentSocket.on("list", (data: AccountConnectionItem[]) => {
        console.log("list", data);
        setAccountConnectionItemList(data);
      });

      return function () {
        currentSocket.disconnect();
        console.log("disconnected!");
      };
    }, []);

    const getRowClass = (item: AccountConnectionItem) => {
      if (item?.connectionState?.label === "active") {
        return "active";
      }
      if (item?.connectionState?.label === "inactive") {
        return "inactive";
      }
      if (item?.connectionState?.label === "away") {
        return "away";
      }
      if (item?.connectionState?.label === "busy") {
        return "busy";
      }
      return "";
    };
    return (
      <Fragment>
        <h3>
          <i className="fas fa-chart-bar me-2"></i>
          <span>Online Monitoring</span>
        </h3>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Position</th>
              <th scope="col">Department</th>
              <th scope="col">Status</th>
            </tr>
          </thead>
          <tbody>
            {connectionItemList?.map((item) => (
              <tr className={getRowClass(item)} key={item?.account?._id}>
                <td>
                  {item?.account?.firstName} {item?.account?.lastName}
                </td>
                <td>{item?.account?.position}</td>
                <td>{item?.account?.department}</td>
                <td className="text-capitalize">
                  {item?.connectionState?.label !== "inactive" && (
                    <span>{item?.connectionState?.label}</span>
                  )}
                  {item?.connectionState?.label === "inactive" && (
                    <span>Recently active</span>
                  )}
                </td>
              </tr>
            ))}
            {!connectionItemList?.length && (
              <tr>
                <td colSpan={4} className="text-center">
                  No data.
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </Fragment>
    );
  };

export default AccountMonitoringList;
