import { Fragment, FunctionComponent, useEffect } from "react";
import { RouteComponentProps, withRouter } from "react-router";
import { useForm } from "react-hook-form";
import { joiResolver } from "@hookform/resolvers/joi";
import "../Authentication.scss";
import { authReset, register as registerUser } from "../../../store/Auth";
import {
  accountSchema,
  DEFAULT_MIN_LENGTH,
  DEFAULT_MAX_LENGTH,
} from "./../../../models/schemas/Account";
import { Account } from "../../../models/interfaces/Account";
import { useDispatch, useSelector } from "react-redux";
import { RegistrationData } from "./../../../models/interfaces/Registration";
import { isAuthProcessing, getAuthError } from "../../../selectors/Auth";
import { isUndefined } from "lodash-es";

interface RegistrationProps extends RouteComponentProps<any> {
  onClickLoginLink: () => void;
}

const Registration: FunctionComponent<RegistrationProps> = ({
  onClickLoginLink,
}) => {
  const dispatch = useDispatch();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Account>({
    resolver: joiResolver(accountSchema),
  });

  const onSubmit = handleSubmit((data: RegistrationData) => {
    dispatch(registerUser(data));
  });

  const processing = useSelector(isAuthProcessing);
  const error = useSelector(getAuthError);

  useEffect(() => {
    dispatch(authReset());
  }, [dispatch]);
  return (
    <Fragment>
      <form onSubmit={onSubmit}>
        <div className="authentication-login d-flex flex-column w-100">
          {!isUndefined(error) && (
            <div className="alert alert-light border border-danger text-danger fw-bold mb-0 py-1 mb-2">
              {error}
            </div>
          )}
          <div className="mb-2">
            <input
              type="text"
              className="form-control"
              placeholder="First name"
              readOnly={processing}
              {...register("firstName", {
                required: true,
                minLength: DEFAULT_MIN_LENGTH,
                maxLength: DEFAULT_MAX_LENGTH,
              })}
            />
            {errors?.firstName?.type && (
              <span className="text-danger text-error">
                {errors?.firstName?.type === "string.empty" &&
                  "First name is required."}
                {errors?.firstName?.type === "string.min" &&
                  `First name should not be less than ${DEFAULT_MIN_LENGTH}`}
                {errors?.firstName?.type === "string.max" &&
                  `First name should not be more than ${DEFAULT_MAX_LENGTH}`}
              </span>
            )}
          </div>
          <div className="mb-2">
            <input
              type="text"
              className="form-control"
              placeholder="Last Name"
              readOnly={processing}
              {...register("lastName", {
                required: true,
                minLength: DEFAULT_MIN_LENGTH,
                maxLength: DEFAULT_MAX_LENGTH,
              })}
            />
            {errors?.lastName?.type && (
              <span className="text-danger text-error">
                {errors?.lastName?.type === "string.empty" &&
                  "Last name is required."}
                {errors?.lastName?.type === "string.min" &&
                  `Last name should not be less than ${DEFAULT_MIN_LENGTH}`}
                {errors?.lastName?.type === "string.max" &&
                  `Last name should not be more than ${DEFAULT_MAX_LENGTH}`}
              </span>
            )}
          </div>
          <div className="mb-2">
            <input
              type="text"
              className="form-control"
              placeholder="Position"
              readOnly={processing}
              {...register("position", {
                maxLength: DEFAULT_MAX_LENGTH,
              })}
            />
            {errors?.position?.type && (
              <span className="text-danger text-error">
                {errors?.position?.type === "string.max" &&
                  `Position should not be more than ${DEFAULT_MAX_LENGTH}`}
              </span>
            )}
          </div>
          <div className="mb-2">
            <input
              type="text"
              className="form-control"
              placeholder="Department"
              readOnly={processing}
              {...register("department", {
                maxLength: DEFAULT_MAX_LENGTH,
              })}
            />
            {errors?.department?.type && (
              <span className="text-danger text-error">
                {errors?.department?.type === "string.max" &&
                  `Department should not be more than ${DEFAULT_MAX_LENGTH}`}
              </span>
            )}
          </div>
          <div className="mb-2">
            <input
              type="email"
              className="form-control"
              id="email"
              placeholder="Email"
              readOnly={processing}
              {...register("email", {
                required: true,
                maxLength: DEFAULT_MAX_LENGTH,
                pattern: {
                  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                  message: "Enter a valid e-mail address",
                },
              })}
            />
            {errors?.email?.type && (
              <span className="text-danger text-error">
                {errors?.email?.type === "string.empty" && "Email is required."}
                {errors?.email?.type === "string.email" && "Email is invalid."}
                {errors?.email?.type === "string.max" &&
                  `Email should not be more than ${DEFAULT_MAX_LENGTH}`}
              </span>
            )}
          </div>
          <div className="mb-2">
            <input
              type="password"
              className="form-control"
              id="password"
              placeholder="Password"
              readOnly={processing}
              {...register("password", {
                required: true,
                minLength: DEFAULT_MIN_LENGTH,
              })}
            />
            {errors?.password?.type && (
              <span className="text-danger text-error">
                {errors?.password?.type === "string.empty" &&
                  "Password is required."}
                {errors?.password?.type === "string.min" &&
                  "Password should not be less than 4."}
              </span>
            )}
          </div>

          <button disabled={processing} className="btn mb-2">
            Submit
          </button>
          <a
            onClick={() => onClickLoginLink()}
            className="text-reset text-center"
          >
            Login
          </a>
        </div>
      </form>
    </Fragment>
  );
};

export default withRouter(Registration);
