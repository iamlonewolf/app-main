import { combineReducers } from "redux";
import accountReducer from "./Account";
export default combineReducers({
  account: accountReducer,
});
