import { createSelector } from "@reduxjs/toolkit";
import { isEqual, isUndefined } from "lodash-es";
import { RootState } from "../models/interfaces/RootState";
import { AuthState } from "../models/interfaces/AuthState";

export const getAuth = ({ auth }: RootState) => auth?.identity;

export const isAuthProcessing = ({ auth }: RootState) => auth?.loading;

export const getAuthError = ({ auth }: RootState) => auth?.error;

export const getAuthIdentity = createSelector(
  ({ auth }: RootState): AuthState | undefined => auth,
  (auth: AuthState | undefined) => {
    if (!auth) {
      return undefined;
    }
    return auth.identity;
  }
);

export const getAccountId = createSelector(getAuthIdentity, (identity) => {
  return identity?._id;
});

export const hasIdentity = (state: RootState) => {
  const identity = getAuthIdentity(state);
  if (isUndefined(identity)) {
    return undefined;
  }
  return !isEqual(identity, false);
};
