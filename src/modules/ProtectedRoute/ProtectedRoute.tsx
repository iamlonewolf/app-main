import { FunctionComponent } from "react";
import { RouteProps } from "react-router";
import { Route, Redirect } from "react-router-dom";
export type ProtectedRouteProps = {
  authenticated?: boolean | undefined;
  redirect?: any;
  component?: any;
} & RouteProps;
const ProtectedRoute: FunctionComponent<ProtectedRouteProps> = ({
  authenticated,
  redirect,
  component: Component,
  ...rest
}: ProtectedRouteProps) => {
  return (
    <Route
      {...rest}
      render={(props) => {
        return authenticated ? (
          <Component {...props} />
        ) : (
          <Redirect to={redirect} />
        );
      }}
    />
  );
};

export default ProtectedRoute;
