export interface Account {
  _id?: string;
  firstName?: string;
  lastName?: string;
  position?: string;
  department?: string;
  email?: string;
  password?: string;
  createdAt?: string;
  updatedAt?: string;
}
