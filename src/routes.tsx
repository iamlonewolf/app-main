export const ROUTE_APP_ROOT = "/";
export const ROUTE_MONITOR_ROOT = "/monitor";
export const ROUTE_AUTHENTICATE_ROOT = "/authenticate";
export const ROUTE_AUTHENTICATE_MODE_ROOT = "/authenticate/:mode";
export const ROUTE_AUTHENTICATE_LOGIN = `${ROUTE_AUTHENTICATE_ROOT}/login`;
export const ROUTE_AUTHENTICATE_REGISTRATION = `${ROUTE_AUTHENTICATE_ROOT}/registration`;
export const ROUTE_ACCOUNT_PROFILE_ROOT = `/profile`;
