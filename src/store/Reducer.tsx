import { combineReducers } from "redux";
import entities from "./Entities";
import auth from "./Auth";
export default combineReducers({
  entities,
  auth,
});
