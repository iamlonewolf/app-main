import { AppState } from "./AppState";
import { Account } from "./Account";

export interface AuthState extends AppState {
  identity?: Account;
}
