import { configureStore } from "@reduxjs/toolkit";
import api from "../middleware/Api";
import reducer from "./Reducer";
// export default function initializeStore() {
//   return configureStore({
//     reducer,
//     middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(api),
//   });
// }
export const store = configureStore({
  reducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(api),
});

export type AppDispatch = typeof store.dispatch;
