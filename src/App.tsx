import React, { Fragment, lazy, Suspense, useEffect } from "react";
import Header from "./modules/Header/Header";
import {
  ROUTE_ACCOUNT_PROFILE_ROOT,
  ROUTE_AUTHENTICATE_LOGIN,
  ROUTE_AUTHENTICATE_MODE_ROOT,
  ROUTE_MONITOR_ROOT,
} from "./routes";
import { Redirect, Route, Switch } from "react-router-dom";
import "./App.css";
import Loading from "./modules/Loading/Loading";
import { authIdentity } from "./store/Auth";
import { useDispatch, useSelector } from "react-redux";
import ProtectedRoute from "./modules/ProtectedRoute/ProtectedRoute";
import { hasIdentity } from "./selectors/Auth";

const AccountMonitoringList = lazy(
  () => import("./modules/AccountMonitoringList/AccountMonitoringList")
);

const AuthenticationLanding = lazy(
  () => import("./modules/AuthenticationLanding/AuthenticationLanding")
);

const AccountProfile = lazy(
  () => import("./modules/AccountProfile/AccountProfile")
);

function App() {
  const dispatch = useDispatch();
  const isLoggedIn = useSelector(hasIdentity);
  useEffect(() => {
    dispatch(authIdentity());
  }, [dispatch]);
  return (
    <Fragment>
      <Header />
      <main className="container h-100">
        <Suspense fallback={<Loading />}>
          {isLoggedIn === undefined && <Loading />}
          {isLoggedIn !== undefined && (
            <Switch>
              <ProtectedRoute
                authenticated={isLoggedIn}
                path={ROUTE_MONITOR_ROOT}
                component={AccountMonitoringList}
                redirect={ROUTE_AUTHENTICATE_LOGIN}
              ></ProtectedRoute>
              <ProtectedRoute
                authenticated={isLoggedIn}
                path={ROUTE_ACCOUNT_PROFILE_ROOT}
                component={AccountProfile}
                redirect={ROUTE_AUTHENTICATE_LOGIN}
              ></ProtectedRoute>
              <Route
                path={ROUTE_AUTHENTICATE_MODE_ROOT}
                component={AuthenticationLanding}
              />
              <Redirect exact from="/" to={ROUTE_AUTHENTICATE_LOGIN} />
            </Switch>
          )}
        </Suspense>
      </main>
    </Fragment>
  );
}

export default App;
