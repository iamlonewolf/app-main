import axios, { AxiosResponse } from "axios";
import { getConfig } from "../Config";
import { StatusCodes } from "http-status-codes";
import { AUTH_TOKEN_KEY, getToken } from "../store/Auth";
import { isNull, isUndefined } from "lodash-es";

axios.interceptors.request.use((config) => {
  const token = getToken();
  if (!isUndefined(token) && !isNull(token)) {
    config.headers.Authorization = token;
  }
  return config;
});

axios.interceptors.response.use(
  (data: AxiosResponse<any>) => {
    console.info("logging response details", data);
    return Promise.resolve(data);
  },
  (error) => {
    for (let key in error) {
      if (typeof error[key] === "function") {
        console.log(key, error[key]());
      } else {
        console.log(key, error[key]);
      }
    }
    const expectedError =
      error.response &&
      error.response.status >= StatusCodes.BAD_GATEWAY &&
      error.response.status < StatusCodes.INTERNAL_SERVER_ERROR;
    if (expectedError) {
      console.error("logging error response details:");
    }
    return Promise.reject(error);
  }
);

export function get(path: string, params: object) {
  const defaultParams = {
    baseURL: getConfig("REACT_APP_API_URL"),
  };
  return axios.get(path, { ...defaultParams, ...params });
}

export function post(path: string, params: object) {
  const defaultParams = {
    baseURL: getConfig("REACT_APP_API_URL"),
  };
  return axios.request({
    ...defaultParams,
    url: path,
    method: "POST",
    data: params,
  });
}

export const setJwt = (token: string) => {
  axios.defaults.headers.common[AUTH_TOKEN_KEY] = token;
};

const httpService = {
  get: get,
  post: post,
  put: axios.put,
  patch: axios.patch,
  delete: axios.delete,
  request: axios.request,
};
export default httpService;
