import { FunctionComponent } from "react";
import Registration from "./Registration/Registration";
import { RouteComponentProps, withRouter } from "react-router";
import Login from "./Login/Login";

export const enum AuthenticationMode {
  LOGIN = "login",
  REGISTRATION = "registration",
}

interface AuthenticationProps extends RouteComponentProps<any> {
  mode: AuthenticationMode;
  onClickLoginLink: () => void;
  onClickRegisterLink: () => void;
}

const Authentication: FunctionComponent<AuthenticationProps> = ({
  mode,
  onClickLoginLink,
  onClickRegisterLink,
}) => {
  return (
    <div>
      <h3 className="h4 text-center mb-3">
        {mode === AuthenticationMode.LOGIN && "Account Login"}
        {mode === AuthenticationMode.REGISTRATION && "Account Registration"}
      </h3>
      {mode === AuthenticationMode.LOGIN && (
        <Login onClickRegisterLink={() => onClickRegisterLink()} />
      )}
      {mode === AuthenticationMode.REGISTRATION && (
        <Registration onClickLoginLink={() => onClickLoginLink()} />
      )}
    </div>
  );
};

export default withRouter(Authentication);
