import { Fragment, FunctionComponent, useEffect } from "react";
import "../Authentication.scss";
import { RouteComponentProps, withRouter } from "react-router";
import { joiResolver } from "@hookform/resolvers/joi";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import {
  accountLoginSchema,
  DEFAULT_MIN_LENGTH,
} from "../../../models/schemas/Account";
import { Account } from "../../../models/interfaces/Account";
import { authReset, login as loginAccount } from "./../../../store/Auth";
import { LoginData } from "./../../../models/interfaces/Login";
import { getAuthError, isAuthProcessing } from "../../../selectors/Auth";
import { getConfig } from "./../../../Config";
import { isUndefined } from "lodash-es";
interface LoginProps extends RouteComponentProps<any> {
  onClickRegisterLink: () => void;
}

const Login: FunctionComponent<LoginProps> = ({ onClickRegisterLink }) => {
  const dispatch = useDispatch();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Account>({
    resolver: joiResolver(accountLoginSchema),
  });

  const onSubmit = handleSubmit((data: LoginData) => {
    dispatch(loginAccount(data));
  });

  const processing = useSelector(isAuthProcessing);
  const error = useSelector(getAuthError);
  useEffect(() => {
    dispatch(authReset());
  }, [dispatch]);
  return (
    <Fragment>
      <form onSubmit={onSubmit}>
        <div className="authentication-login d-flex flex-column w-100">
          {!isUndefined(error) && (
            <div className="alert alert-light border border-danger text-danger fw-bold mb-0 py-1 mb-2">
              {error}
            </div>
          )}
          <div className="mb-2">
            <div className="input-group">
              <span className="input-group-text">
                <i className="fas fa-user-circle"></i>
              </span>
              <input
                type="email"
                id="email"
                className="form-control"
                readOnly={processing}
                placeholder="Email"
                {...register("email", {
                  required: true,
                  pattern: {
                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                    message: "Enter a valid e-mail address",
                  },
                })}
              />
            </div>
            {errors?.email?.type && (
              <span className="text-danger text-error">
                {errors?.email?.type === "string.empty" && "Email is required."}
                {errors?.email?.type === "string.email" && "Email is invalid."}
              </span>
            )}
          </div>
          <div className="mb-2">
            <div className="input-group">
              <span className="input-group-text">
                <i className="fas fa-key"></i>
              </span>
              <input
                type="password"
                id="password"
                className="form-control"
                placeholder="Password"
                readOnly={processing}
                {...register("password", {
                  required: true,
                  minLength: DEFAULT_MIN_LENGTH,
                })}
              />
            </div>
            {errors?.password?.type && (
              <span className="text-danger text-error">
                {errors?.password?.type === "string.empty" &&
                  "Password is required."}
                {errors?.password?.type === "string.min" &&
                  "Password should not be less than 4."}
              </span>
            )}
          </div>
          <button disabled={processing} className="btn mb-2">
            Submit
          </button>
          <a
            onClick={() => onClickRegisterLink()}
            className="text-reset text-center"
          >
            Register
          </a>
        </div>
      </form>
    </Fragment>
  );
};

export default withRouter(Login);
