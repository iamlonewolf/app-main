import { createAsyncThunk } from "@reduxjs/toolkit";
import { isEmpty } from "lodash-es";
import { getConfig } from "../Config";
import httpService from "../services/HttpService";
import { APIConfig } from "./Api";

export const apiCall = createAsyncThunk(
  "api/call",
  async (config: APIConfig, { dispatch, rejectWithValue }) => {
    const {
      url,
      method,
      postParams,
      queryParams,
      onStart,
      onSuccess,
      onError,
      onDone,
    } = config;

    try {
      if (onStart) {
        dispatch({
          type: onStart,
        });
      }
      const { data, headers } = await httpService.request({
        baseURL: getConfig("REACT_APP_API_URL"),
        url,
        method: method ?? "GET",
        data: postParams,
        params: queryParams,
      });

      if (onSuccess) {
        dispatch({
          type: onSuccess,
          payload: {
            data,
            headers,
          },
        });
      }
      return { data, headers };
    } catch (error: any) {
      const { response } = error;
      const responseData = !isEmpty(response?.data)
        ? response?.data
        : response?.statusText;
      if (onError) {
        dispatch({
          type: onError,
          payload: responseData,
        });
      }
      return rejectWithValue(responseData);
    } finally {
      if (onDone) {
        dispatch({
          type: onDone,
        });
      }
    }
  }
);

// export const getData = apiCall({});
// const slice = createSlice({
//   name: "dummy",
//   initialState: {
//     list: [],
//   },
//   reducers: {},
//   extraReducers: (builder) => {
//     builder.addCase(apiCall.pending, () => {});
//     builder.addCase(apiCall.rejected, () => {});
//   },
// });

// export default slice.reducer;
